
## System Requirements:

To use CUDA on your system, you will need a CUDA-capable GPU (Nvidia GEForce GTX 950 (or higher).

---

## Requirements

The following requirements must be installed:

1. Anaconda version 4.7.11.
2. Python version 3.7.0.
3. Cuda Toolkit version 9.0.
4. cuDNN version 7.1.4.
5. keras gpu 2.2.4.

###### The easiest way to install 3, 4 and 5:
```
conda create --name tf_gpu tensorflow-gpu
```

---
### Download data sets

1. Download both data sets (mPower and FinalHar) from:
https://mega.nz/#F!bdEWiSYR!smdR2-C4UftatrqxBxOhYg
2. Change the path for the mPower data set in helpers.py -> read_mPowerAll()
```
  data_path = 'yourPath\\mPower\\'
   mPower = pd.read_csv(data_path+'patientsMeasuresSymptoms.csv', index_col=0)
```

### Models
You can view all .ipynb files using the jupyter notebook viewer:
https://nbviewer.jupyter.org/github/andrejpetrusevski/bachelor_thesis/tree/master/  

Different types of models are located in different files:

#### 1. mPower_LSTM.ipynb
1. LSTM
2. LSTM+1FC
3. LSTM+2FC
4. LSTM+3FC
5. StackedLSTM2+1FC
6. StackedLSTM3+1FC  
7. 256 segments length version of StackedLSTM2+1FC  


#### 2. mPower_CNN.ipynb
 1. 2x1CNN+2FC
 2. 2x1CNN+4FC
 3. 3x2CNN+4FC
 4. 256 segment length version of 2x1CNN+2FC

#### 3. mPower128_TransferCNN.ipynb
1. 2x1CNN+2FC - retrained only fc layers
2. 2x1CNN+4FC - retrained second block and FC
3. 3x2CNN+4FC - all layers (50 epochs)

#### 4. mPower128_TransferLSTM.ipynb
1. StackedLSTM2+1FC fine tuned  

#### 5. RWS_byPatients.ipynb
1. StackedLSTM2+1FC for symptom 2.12
2. 2x1CNN+2FC for symptom 2.12
3. StackedLSTM2+1FC for symptom 2.13
4. 2x1CNN+2FC for symptom 2.13
