import mobiAct_preprocess as mobi
import uciData_preprocess as uci
import mPower_preprocess as mp
from scipy import stats

from importlib import reload
reload(mobi)
reload(uci)
reload(mp)

import numpy as np
import pandas as pd
import operator
from matplotlib import pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder

def read_MobiAct(test_size, state):
    X,y = mobi.read_mobiSegments()
    mobiX_train, mobiX_test, mobiY_train, mobiY_test = train_test_split(X, y, test_size=test_size, random_state=state)
    return mobiX_train, mobiX_test, mobiY_train, mobiY_test

def read_UciHar():
    uciX, uciY, uciTestX, uciTesty = uci.load_dataset()
    # convert labels to be compatible with mobiAct labels
    uciY_train = list()
    uciY_test = list()
    for i in range(0, len(uciY)):
        if uciY[i][0] == 0:
            uciY_train.append("WAL")
        if uciY[i][0] == 1:
            uciY_train.append("STU")
        if uciY[i][0] == 2:
            uciY_train.append("STN")
        if uciY[i][0] == 3:
            uciY_train.append("SIT")
        if uciY[i][0] == 4:
            uciY_train.append("STD")
        if uciY[i][0] == 5:
            uciY_train.append("LAY")       

    for i in range(0, len(uciTesty)):
        if uciTesty[i][0] == 0:
            uciY_test.append("WAL")
        if uciTesty[i][0] == 1:
            uciY_test.append("STU")
        if uciTesty[i][0] == 2:
            uciY_test.append("STN")
        if uciTesty[i][0] == 3:
            uciY_test.append("SIT")
        if uciTesty[i][0] == 4:
            uciY_test.append("STD")
        if uciTesty[i][0] == 5:
            uciY_test.append("LAY")

    uciY_test = np.asarray(uciY_test).reshape(len(uciY_test),1)
    uciY_train = np.asarray(uciY_train).reshape(len(uciY_train),1)

    return uciX, uciTestX, uciY_train, uciY_test

def read_prepareHarData():
    mobiX_train, mobiX_test, mobiY_train, mobiY_test = read_MobiAct(0.3,7)
    uciX_train, uciX_test, uciY_train, uciY_test = read_UciHar()

    finalX_train = np.vstack((mobiX_train, uciX_train))
    finalX_test = np.vstack((mobiX_test, uciX_test))
    finalY_train = np.vstack((mobiY_train,uciY_train))
    finalY_test = np.vstack((mobiY_test,uciY_test))

    print("OneHot encoding ... ")
    finalY_train, classesHarTrain = one_hot_encoding(finalY_train,False)
    finalY_test, classesHarTest = one_hot_encoding(finalY_test,False)
    print("--------------------------------------------------------------")
    print("--------------------------------------------------------------")
    return finalX_train, finalX_test, finalY_train, finalY_test


def read_mPowerAll(sequence_length, ov_step, merge, thresh):
    data_path = 'D:\\Diploma_code\\datasets\\mPower\\'
    mPower = pd.read_csv(data_path+'patientsMeasuresSymptoms.csv', index_col=0)
    
    if merge == True: 
        mPower = mp.mergeLabels(mPower)
        
    # normalize mPower and drop columns 'healthCode' and 'timestamp'
    #mPower = normalize(mPower)
    
    #mPower = mPower.drop(['healthCode', 'timestamp'], axis=1)
    print("Reshaping started ...")
    reshaped_TrainSegments, train_labels27, train_labels210, train_labels212, train_labels213, train_hcs = mp.create_segments_and_labels(mPower, sequence_length, ov_step, '', False, thresh)


    trainLabels = pd.DataFrame(
        {'27': train_labels27,
         '210': train_labels210,
         '212': train_labels212,
         '213': train_labels213
        })

    
    return reshaped_TrainSegments, trainLabels, train_hcs

def normalize(df):
    df['x_acc'] = df['x_acc'] / df['x_acc'].max()
    df['y_acc'] = df['y_acc'] / df['y_acc'].max()
    df['z_acc'] = df['z_acc'] / df['z_acc'].max()

    df['x_rot'] = df['x_rot'] / df['x_rot'].max()
    df['y_rot'] = df['y_rot'] / df['y_rot'].max()
    df['z_rot'] = df['z_rot'] / df['z_rot'].max()
    
    return df

def one_hot_encoding(integer_encoded, reshape):
    onehot_encoder = OneHotEncoder(sparse=False)
    if reshape:
        integer_encoded = integer_encoded.reshape(1,len(integer_encoded))[0]
    onehot_encoded = onehot_encoder.fit_transform(integer_encoded)
    classes = onehot_encoder.inverse_transform(onehot_encoded)
    return onehot_encoded,classes


def visualize_accuracy(acc, val_acc):
    plt.figure(figsize=(10, 6))
    plt.plot(acc, 'r', label='Accuracy of training data')
    plt.plot(val_acc, 'b', label='Accuracy of validation data')
    plt.title('Model Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Training Epoch')
    plt.ylim(0)
    plt.legend()
    plt.show()
    

def visualize_loss(history):
    plt.figure(figsize=(10, 6))
    plt.plot(history.history['loss'], 'r--', label='Loss of training data')
    plt.plot(history.history['val_loss'], 'b--', label='Loss of validation data')
    plt.title('Model Loss')
    plt.ylabel('Loss')
    plt.xlabel('Training Epoch')
    plt.ylim(0)
    plt.legend()
    plt.show()
    
def countPatientsBySymptom(realValues, symptom, estimation):
    count = 0
    for pat in realValues:
        if realValues[pat][symptom] == estimation:
            count+=1
    return count
    
def classificationAccuracy_patients213(model, reshaped_TestSegments, test_hcs, testLabels):
    start = 1
    result213 = 0
    res213_0, res213_1, res213_2, res213_3 = 0, 0, 0, 0

    
    realValues = {}
    predictedValues = {}
    hc = test_hcs[0]
    add = len([i for i in test_hcs if str(i) == str(hc)])
    while start <= len(test_hcs):
        pv27 = []
        hc = test_hcs[start-1]
        add = len([i for i in test_hcs if str(i) == str(hc)])
        realValues[hc] = [testLabels['213'][start-1]]
        ty_probmPower213 = model.predict(reshaped_TestSegments[start-1:(start-1+add)])
        
        for item in ty_probmPower213:
            if max(item) > 0.0:
                pv27.append(item.argmax(-1))
        print("probmPower27.argmax(-1)[0][0] = " + str(stats.mode(pv27))+ " of "+ str(len(pv27)))

        predictedValues[hc] = [longestSubSeq(pv27)]
        #predictedValues[hc] = [stats.mode(ty_probmPower213.argmax(-1))[0][0]]

        if realValues[hc][0] == predictedValues[hc][0]:
            result213 += 1
            if realValues[hc][0] == 1:
                res213_1 += 1
            elif realValues[hc][0] == 2:
                res213_2 += 1
            elif realValues[hc][0] == 3:
                res213_3 += 1
            else:
                res213_0 += 1

        print("HealthCode : % s, Real Values : % s, Predicted: %s \n" %(hc, realValues[hc], predictedValues[hc])) 
        start+=add
    
    all_patients = len(set(test_hcs))
    
    print("Symptom : 2.13, Number of patients : % s Positive %s  Ratio : %s \n" %(all_patients, result213, result213/all_patients)) 
    print("\t Symptom 2.13 positive prediction by values: \n\t\t\t\t\t"
                                                  "Zero : %s of %s \n\t\t\t\t\t"
                                                  "One : %s of %s \n\t\t\t\t\t"
                                                  "Two : %s of %s \n\t\t\t\t\t"
                                                  "Three : %s of %s \n" %(res213_0, countPatientsBySymptom(realValues, 0, 0),
                                                                      res213_1, countPatientsBySymptom(realValues, 0, 1),
                                                                      res213_2, countPatientsBySymptom(realValues, 0, 2),
                                                                      res213_3, countPatientsBySymptom(realValues, 0, 3))) 
    
def classificationAccuracy_patients212(model, reshaped_TestSegments, test_hcs, testLabels):
    start = 1
    result213 = 0
    res213_0, res213_1, res213_2, res213_3 = 0, 0, 0, 0

    
    realValues = {}
    predictedValues = {}
    hc = test_hcs[0]
    add = len([i for i in test_hcs if str(i) == str(hc)])
    while start <= len(test_hcs):
        pv27 = []
        hc = test_hcs[start-1]
        add = len([i for i in test_hcs if str(i) == str(hc)])
        realValues[hc] = [testLabels['212'][start-1]]
        ty_probmPower212 = model.predict(reshaped_TestSegments[start-1:(start-1+add)])
        
        for item in ty_probmPower212:
            if max(item) > 0.0:
                pv27.append(item.argmax(-1))
       # predictedValues[hc] = [stats.mode(ty_probmPower212.argmax(-1))[0][0]]
        print("probmPower27.argmax(-1)[0][0] = " + str(stats.mode(pv27))+ " of "+ str(len(pv27)))

        predictedValues[hc] = [longestSubSeq(pv27)]
        if realValues[hc][0] == predictedValues[hc][0]:
            result213 += 1
            if realValues[hc][0] == 1:
                res213_1 += 1
            elif realValues[hc][0] == 2:
                res213_2 += 1
            elif realValues[hc][0] == 3:
                res213_3 += 1
            else:
                res213_0 += 1

        print("HealthCode : % s, Real Values : % s, Predicted: %s \n" %(hc, realValues[hc], predictedValues[hc])) 
        start+=add
    
    all_patients = len(set(test_hcs))
    
    print("Symptom : 2.13, Number of patients : % s Positive %s  Ratio : %s \n" %(all_patients, result213, result213/all_patients)) 
    print("\t Symptom 2.13 positive prediction by values: \n\t\t\t\t\t"
                                                  "Zero : %s of %s \n\t\t\t\t\t"
                                                  "One : %s of %s \n\t\t\t\t\t"
                                                  "Two : %s of %s \n\t\t\t\t\t"
                                                  "Three : %s of %s \n" %(res213_0, countPatientsBySymptom(realValues, 0, 0),
                                                                      res213_1, countPatientsBySymptom(realValues, 0, 1),
                                                                      res213_2, countPatientsBySymptom(realValues, 0, 2),
                                                                      res213_3, countPatientsBySymptom(realValues, 0, 3)))
    
def longestSubSeq(seq):
    result=1
    max_result=0
    last_seen=seq[0]
    number=0
    dicta = {}

    for v in seq[1:]:
        if v==last_seen:
            result += 1
        else:
            dicta[last_seen] = result
            last_seen = v
            result = 1
    dicta[last_seen] = result
    return max(dicta.items(), key=operator.itemgetter(1))[0]


'''
def read_mPower(sequence_length, ov_step, merge, thresh, twelve):
    data_path = 'D:\\Diploma_code\\datasets\\mPower\\'
    mPower = pd.read_csv(data_path+'patientsMeasuresSymptoms.csv', index_col=0)
    
    print(twelve)
    if merge == True: 
        mPower = mp.mergeLabels(mPower)

    print("Reshaping started ...")
    if twelve == True:
        reshaped_TrainSegments, train_labels212, train_hcs = mp.create_segments_and_labels_212(mPower, sequence_length, ov_step, '', False, thresh)
        trainLabels = pd.DataFrame(
            {
             '212': train_labels212,
            })
    else:
        reshaped_TrainSegments, train_labels27, train_labels210, train_labels212, train_labels213, train_hcs = mp.create_segments_and_labels(mPower, sequence_length, ov_step, '', False, thresh)
        trainLabels = pd.DataFrame(
            {'27': train_labels27,
             '210': train_labels210,
             '212': train_labels212,
             '213': train_labels213
            })

    
    return reshaped_TrainSegments, trainLabels, train_hcs
'''