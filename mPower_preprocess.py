import os
import shutil
from progressbar import ProgressBar, Percentage, Bar, ETA
from scipy import stats
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
import itertools    
from sklearn.preprocessing import LabelEncoder
import mobiAct_preprocess as mobi
from pandas import Series
from sklearn.preprocessing import MinMaxScaler

def __get_symptoms(_syn):
    # This gets MDS-UPDRS table
    _query = _syn.tableQuery("select * FROM syn5511432")
    mds_updrs = _query.asDataFrame()

    # Retrieve health codes (a.k.a. patient id)
    _healthCodes = mds_updrs["healthCode"]

    # Retrieve symptoms from the table.
    # It is a dictionary of (symp1, symp2, symp3...) with patient ids as keys.
    patients = {}
    for h in _healthCodes:
        two_7 = mds_updrs.loc[mds_updrs['healthCode'] == h]["MDS-UPDRS2.7"][0]
        two_8 = mds_updrs.loc[mds_updrs['healthCode'] == h]["MDS-UPDRS2.8"][0]
        two_12 = mds_updrs.loc[mds_updrs['healthCode'] == h]["MDS-UPDRS2.12"][0]
        two_13 = mds_updrs.loc[mds_updrs['healthCode'] == h]["MDS-UPDRS2.13"][0]

        # save the symptoms for every patient
        patients[h] = [two_7 , two_8, two_12, two_13]
        
    return patients

def normalize_zero_one(segment):
    series = Series(segment)
    # prepare data for normalization
    values = series.values
    values = values.reshape((len(values), 1))

    # train the normalization
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaler = scaler.fit(values)
    # normalize the dataset
    return scaler.transform(values)

def create_segments_and_labels(df, time_steps, step, path, pics, thresh):
    # x, y, z acceleration as features
    N_FEATURES = 6
    counter = 0
    add = 0
    allzeroes = 0
    temphc = stats.mode(df["healthCode"].values[0: 0 + time_steps])[0][0]
    
    # Number of steps to advance in each iteration (for me, it should always
    # be equal to the time_steps in order to have no overlap between segments)
    # step = time_steps
    segments = []
    labels27 = []
    labels210 = []
    labels212 = []
    labels213 = []
    hcs = []

    for i in range(0, len(df) - time_steps, step):
        
        xa = normalize_zero_one(df['x_acc'].values[i: i + time_steps])
        xr = normalize_zero_one(df['x_rot'].values[i: i + time_steps])
        ya = normalize_zero_one(df['y_acc'].values[i: i + time_steps])
        yr = normalize_zero_one(df['y_rot'].values[i: i + time_steps])
        za = normalize_zero_one(df['z_acc'].values[i: i + time_steps])
        zr = normalize_zero_one(df['z_rot'].values[i: i + time_steps])

        # Retrieve the most often used label in this segment
        label27  = stats.mode(df["two_7"][i: i + time_steps])
        label210 = stats.mode(df["two_10"][i: i + time_steps])
        label212 = stats.mode(df["two_12"][i: i + time_steps])
        label213 = stats.mode(df["two_13"][i: i + time_steps])
        hc = stats.mode(df["healthCode"].values[i: i + time_steps])
        
        if (label27[1][0] < thresh or label210[1][0] < thresh or label212[1][0] < thresh or label213[1][0] < thresh or hc[1][0] < thresh):
            counter+=1
            #print("NOT OK:")
            continue

        if(path != ''):
            pd.DataFrame([xa]).to_csv(path+'\\body_acc_x.txt', mode='a', index=False, header = None)
            pd.DataFrame([ya]).to_csv(path+'\\body_acc_y.txt', mode='a', index=False, header = None)
            pd.DataFrame([za]).to_csv(path+'\\body_acc_z.txt', mode='a', index=False, header = None)
            pd.DataFrame([xr]).to_csv(path+'\\body_gyro_x.txt', mode='a', index=False, header = None)
            pd.DataFrame([yr]).to_csv(path+'\\body_gyro_y.txt', mode='a', index=False, header = None)
            pd.DataFrame([zr]).to_csv(path+'\\body_gyro_z.txt', mode='a', index=False, header = None)

        #print(add)
        
        if label27[0][0] == 0 and label213[0][0] == 0 and label212[0][0] == 0 and label210[0][0] == 0: 
            if allzeroes < 100 and hc[0][0] == temphc:
                print(str(hc[0][0]) + " "+ str(temphc) + " " + str(allzeroes))
                segments.append([xa, ya, za, xr, yr, zr])
                labels27.append(label27[0][0])
                labels210.append(label210[0][0])
                labels212.append(label212[0][0])
                labels213.append(label213[0][0])
                hcs.append(hc[0][0])
            else:
                allzeroes+=1
                temphc = hc[0][0]
                print("temphc")

        else:
            segments.append([xa, ya, za, xr, yr, zr])

            labels27.append(label27[0][0])
            labels210.append(label210[0][0])
            labels212.append(label212[0][0])
            labels213.append(label213[0][0])
            hcs.append(hc[0][0])
            
            if pics == True:
                fig, axs = plt.subplots(2, 3)
                axs[0, 0].plot(xa)
                axs[0, 0].set_title('xa')

                axs[0, 1].plot(ya, 'tab:orange')
                axs[0, 1].set_title('ya')

                axs[0, 2].plot(za, 'tab:red')
                axs[0, 2].set_title('za')

                axs[1, 0].plot(xr, 'tab:blue')
                axs[1, 0].set_title('za')

                axs[1, 1].plot(yr, 'tab:orange')
                axs[1, 1].set_title('xr')

                axs[1, 2].plot(zr, 'tab:red')
                axs[1, 2].set_title('xr')

                # Hide x labels and tick labels for top plots and y ticks for right plots.
                for ax in axs.flat:
                    ax.label_outer()

                p='D:\\Diploma_code\\pics\\'
                name = p + str(i)
                est = str(label27[0][0]) + " " + str(label210[0][0]) + " " + str(label212[0][0]) + " " + str(label213[0][0]) 
                plt.title(est)
                plt.savefig(name)

        
    
        
    # Bring the segments into a better shape
    reshaped_segments = np.asarray(segments, dtype= np.float32).reshape(-1, time_steps, N_FEATURES)
    labels27 = np.asarray(labels27)
    labels210 = np.asarray(labels210)
    labels212 = np.asarray(labels212)
    labels213 = np.asarray(labels213)

    print("Deleted segments: " + str(counter))
    return reshaped_segments, labels27, labels210, labels212, labels213, hcs


def create_segments_and_labels_212(df, time_steps, step, path, pics, thresh):
    # x, y, z acceleration as features
    N_FEATURES = 6
    counter = 0
    add = 0
    zeroes = 0
    ones = 0
    twos = 0
    twoseg = 0
    temphc0 = stats.mode(df["healthCode"].values[0: 0 + time_steps])[0][0]
    temphc1 = stats.mode(df["healthCode"].values[0: 0 + time_steps])[0][0]
    temphc2 = stats.mode(df["healthCode"].values[0: 0 + time_steps])[0][0]
    # Number of steps to advance in each iteration (for me, it should always
    # be equal to the time_steps in order to have no overlap between segments)
    # step = time_steps
    segments = []
    labels212 = []
    hcs = []

    for i in range(0, len(df) - time_steps, step):
        
        xa = normalize_zero_one(df['x_acc'].values[i: i + time_steps])
        xr = normalize_zero_one(df['x_rot'].values[i: i + time_steps])
        ya = normalize_zero_one(df['y_acc'].values[i: i + time_steps])
        yr = normalize_zero_one(df['y_rot'].values[i: i + time_steps])
        za = normalize_zero_one(df['z_acc'].values[i: i + time_steps])
        zr = normalize_zero_one(df['z_rot'].values[i: i + time_steps])
        

        label212 = stats.mode(df["two_12"][i: i + time_steps])
        hc = stats.mode(df["healthCode"].values[i: i + time_steps])
        
        if ( label212[1][0] < thresh or hc[1][0] < thresh):
            counter+=1
            #print("NOT OK:")
            continue

        
        if label212[0][0] == 0: 
            if zeroes < 80 and hc[0][0] == temphc0:
                print(str(hc[0][0]) + " "+ str(temphc0) + " " + str(zeroes))
                segments.append([xa, ya, za, xr, yr, zr])
                labels212.append(label212[0][0])
                hcs.append(hc[0][0])
            else:
                zeroes+=1
                temphc0 = hc[0][0]
                print("temphc0")
                
        elif label212[0][0] == 1:
            if zeroes < 50 and hc[0][0] == temphc1:
                print(str(hc[0][0]) + " "+ str(temphc1) + " " + str(ones))
                segments.append([xa, ya, za, xr, yr, zr])
                labels212.append(label212[0][0])
                hcs.append(hc[0][0])
            else:
                ones+=1
                temphc1 = hc[0][0]
                print("temphc1")
        elif label212[0][0] == 2:
            if twos < 41 and hc[0][0] == temphc2:
                if twoseg < 80:
                    print(str(hc[0][0]) + " "+ str(temphc1) + " " + str(ones))
                    segments.append([xa, ya, za, xr, yr, zr])
                    labels212.append(label212[0][0])
                    hcs.append(hc[0][0])
                    twoseg+=1
            else:
                twos+=1
                temphc2 = hc[0][0]
                twoseg = 0
                print("temphc1")
        else:
            segments.append([xa, ya, za, xr, yr, zr])
            labels212.append(label212[0][0])
            hcs.append(hc[0][0])

    # Bring the segments into a better shape
    reshaped_segments = np.asarray(segments, dtype= np.float32).reshape(-1, time_steps, N_FEATURES)
    labels212 = np.asarray(labels212)
    
    print("Deleted segments: " + str(counter))
    return reshaped_segments, labels212, hcs


def read_mPowerSegments():
    # load all data
    X = load_dataset_group()
    print(X.shape)
    return X
    
def load_file(filepath):
    dataframe = pd.read_csv(filepath, header=None)
    return dataframe.values

# load a list of files into a 3D array of [samples, timesteps, features]
def load_group(filenames, prefix=''):
    loaded = list()
    for name in filenames:
        data = load_file(prefix + name)
        loaded.append(data)
    # stack group so that features are the 3rd dimension
    loaded = np.dstack(loaded)
    return loaded

def load_dataset_group():
    filepath = 'D:\\pd\\datasets\\finalPatients\\'
    # load all 6 files as a single array
    filenames = list()
    # body acceleration
    filenames += ['body_acc_x.txt', 'body_acc_y.txt', 'body_acc_z.txt']
    # body gyroscope
    filenames += ['body_gyro_x.txt', 'body_gyro_y.txt', 'body_gyro_z.txt']
    # load input data
    X = load_group(filenames, filepath)
    # load class output
    return X

def mergeLabels(dataset):
    
    '''dataset.loc[dataset['two_7'] <= 1, 'two_7'] = 1
    dataset.loc[dataset['two_10'] <= 1, 'two_10'] = 1
    dataset.loc[dataset['two_12'] <= 1, 'two_12'] = 1
    dataset.loc[dataset['two_13'] <= 1, 'two_13'] = 1
    
    dataset.loc[(dataset['two_7'] >= 1) & (dataset['two_7'] <= 2), 'two_7'] = 1
    dataset.loc[(dataset['two_10'] >= 1) & (dataset['two_10'] <= 2), 'two_10'] = 1
    dataset.loc[(dataset['two_12'] >= 1) & (dataset['two_12'] <= 2), 'two_12'] = 1
    dataset.loc[(dataset['two_13'] >= 1) & (dataset['two_13'] <= 2), 'two_13'] = 1
    '''
    
    dataset.loc[(dataset['two_7'] >= 3 ), 'two_7'] = 3
    dataset.loc[(dataset['two_10'] >= 3), 'two_10'] = 3
    dataset.loc[(dataset['two_12'] >= 3), 'two_12'] = 3
    dataset.loc[(dataset['two_13'] >= 3), 'two_13'] = 3
    
    
    return dataset
    
def calc_confusion_matrix_mPower(y_probmPower, trueClasses, merged):
    y_classesmPower = y_probmPower.argmax(axis=-1)
    if merged == True:
        class_names = ['Normal', 'Moderate', 'Severe']
    else:
        class_names = ['Normal', 'Slight','Mild', 'Moderate/Severe']

    # Compute confusion matrix
    cm = confusion_matrix( trueClasses, y_classesmPower)

    plt.rcParams['figure.figsize'] = [7, 5]
    plt.style.use('ggplot')
    mobi.plot_confusion_matrix(cm,class_names, True)

    print(classification_report(trueClasses, y_classesmPower, target_names=class_names))

