from numpy import mean
from numpy import std
from numpy import dstack
from pandas import read_csv
from keras.utils import to_categorical
from matplotlib import pyplot

def load_file(filepath):
    dataframe = read_csv(filepath, header=None, delim_whitespace=True)
    return dataframe.values
	
# load a list of files into a 3D array of [samples, timesteps, features]
def load_group(filenames, prefix=''):
    loaded = list()
    for name in filenames:
        data = load_file(prefix + name)
        loaded.append(data)
    # stack group so that features are the 3rd dimension
    loaded = dstack(loaded)
    return loaded
	
def load_dataset_group(group, prefix=''):
    filepath = prefix + group + '/Inertial Signals/'
    # load all 9 files as a single array
    filenames = list()
    # total acceleration
    #filenames += ['total_acc_x_'+group+'.txt', 'total_acc_y_'+group+'.txt', 'total_acc_z_'+group+'.txt']
    # body acceleration
    filenames += ['body_acc_x_'+group+'.txt', 'body_acc_y_'+group+'.txt', 'body_acc_z_'+group+'.txt']
    # body gyroscope
    filenames += ['body_gyro_x_'+group+'.txt', 'body_gyro_y_'+group+'.txt', 'body_gyro_z_'+group+'.txt']
    # load input data
    X = load_group(filenames, filepath)
    # load class output
    y = load_file(prefix + group + '/y_'+group+'.txt')
    return X, y
	
def load_dataset(prefix=''):
    # load all train
    trainX, trainy = load_dataset_group('train', 'D:\\Diploma_code\\bachelorthesis\\datasets\\finalHar\\UCI HAR Dataset\\')
    # load all test
    testX, testy = load_dataset_group('test', 'D:\\Faks\\diploma\\harData\\UCI HAR Dataset\\')
    # zero-offset class values
    trainy = trainy - 1
    testy = testy - 1
    # one hot encode y
    #trainy = to_categorical(trainy)
    #testy = to_categorical(testy)
    print("UciHar train shape (x) -  (y) : " + str(trainX.shape) + " - " + str(trainy.shape))
    print("UciHar test shape (x) -  (y) : " + str(testX.shape) + " -  " + str(testy.shape))
    print("--------------------------------------------------------------")
    print("--------------------------------------------------------------")
    return trainX, trainy, testX, testy
	
