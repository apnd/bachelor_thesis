import pandas as pd
import numpy as np
import json
import os
from scipy import stats
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from matplotlib import pyplot as plt
import uciData_preprocess as uci
import itertools    
from sklearn.metrics import classification_report 
import seaborn as sns
sns.set()

# Merge all small parts in one big dataset
def createMobiActDataset(data_path):
    finalMobi = pd.DataFrame(columns = ['timestamp', 'x_acc','y_acc','z_acc','x_rot','y_rot','z_rot', 'label'])
    for subdir, dirs, files in os.walk(data_path, topdown=True):
        for file in files:
            tmp_df = pd.read_csv(subdir + '\\' + file)
            tmp_df = tmp_df.drop(["rel_time", "azimuth", "pitch", "roll"], axis=1)
            tmp_df.rename(
                columns={
                    "acc_x": "x_acc",
                    "acc_y": "y_acc",
                    "acc_z": "z_acc",
                    "gyro_x": "x_rot",
                    "gyro_y": "y_rot",
                    "gyro_z": "z_rot"
                }, inplace=True)
            print(file)
            finalMobi = finalMobi.append(tmp_df)
    return finalMobi


def downSampleMobiAct(dataframe):
    firstHalf = dataframe.iloc[::2, :]
    secondHalf = dataframe.iloc[1::2,:]

    first4 = firstHalf.iloc[::2,:]
    second4 = firstHalf.iloc[1::2,:]
    third4 = secondHalf.iloc[::2,:]
    forth4 = secondHalf.iloc[1::2,:]
    
    first4 = first4.append(second4)
    third4 = third4.append(forth4)

    first4 = first4.append(third4)

    return first4

def create_segments_and_labels(df, time_steps, step, path):

    # x, y, z acceleration as features
    N_FEATURES = 6
    counter = 0
    
    # Number of steps to advance in each iteration (for me, it should always
    # be equal to the time_steps in order to have no overlap between segments)
    # step = time_steps
    segments = []
    labels = []
    for i in range(0, len(df) - time_steps, step):
        xa = df['x_acc'].values[i: i + time_steps]
        xr = df['x_rot'].values[i: i + time_steps]
        ya = df['y_acc'].values[i: i + time_steps]
        yr = df['y_rot'].values[i: i + time_steps]
        za = df['z_acc'].values[i: i + time_steps]
        zr = df['z_rot'].values[i: i + time_steps]

        # Retrieve the most often used label in this segment
        label = stats.mode(df["label"][i: i + time_steps])
        if label[1][0] < 100:
            counter+=1
            print("NOT OK:")
            print(counter)
            continue
        if(path != ''):
            pd.DataFrame([xa]).to_csv(path+'\\body_acc_x.txt', mode='a', index=False, header = None)
            pd.DataFrame([ya]).to_csv(path+'\\body_acc_y.txt', mode='a', index=False, header = None)
            pd.DataFrame([za]).to_csv(path+'\\body_acc_z.txt', mode='a', index=False, header = None)
            pd.DataFrame([xr]).to_csv(path+'\\body_gyro_x.txt', mode='a', index=False, header = None)
            pd.DataFrame([yr]).to_csv(path+'\\body_gyro_y.txt', mode='a', index=False, header = None)
            pd.DataFrame([zr]).to_csv(path+'\\body_gyro_z.txt', mode='a', index=False, header = None)

        segments.append([xa, ya, za, xr, yr, zr])
        labels.append(label[0][0])

    # Bring the segments into a better shape
    reshaped_segments = np.asarray(segments, dtype= np.float32).reshape(-1, time_steps, N_FEATURES)
    labels = np.asarray(labels)

    return reshaped_segments, labels
  
def read_mobiSegments():
    # load all data
    X, y = load_dataset_group()
    print("--------------------------------------------------------------")
    print("--------------------------------------------------------------")
    print("MobiAct shape (x) - (y) : " + str(X.shape) + " - " + str(y.shape))
    print("--------------------------------------------------------------")
    print("--------------------------------------------------------------")
    return X,y
    
def load_file(filepath):
    dataframe = pd.read_csv(filepath, header=None)
    return dataframe.values

# load a list of files into a 3D array of [samples, timesteps, features]
def load_group(filenames, prefix=''):
    loaded = list()
    for name in filenames:
        data = load_file(prefix + name)
        loaded.append(data)
    # stack group so that features are the 3rd dimension
    loaded = np.dstack(loaded)
    return loaded

def load_dataset_group():
    filepath ='D:\\Diploma_code\\bachelorthesis\\datasets\\finalHar\\MobiInertialSignals\\' 
    # load all 6 files as a single array
    filenames = list()
    # body acceleration
    filenames += ['body_acc_x.txt', 'body_acc_y.txt', 'body_acc_z.txt']
    # body gyroscope
    filenames += ['body_gyro_x.txt', 'body_gyro_y.txt', 'body_gyro_z.txt']
    # load input data
    X = load_group(filenames, filepath)
    # load class output
    y = load_file(filepath + '\\labels.txt')
    return X, y

def plot_activity(activity, data):

    fig, (ax0, ax1, ax2) = plt.subplots(nrows=3,
         figsize=(5, 5),
         sharex=True)
    plot_axis(ax0, data['timestamp'], data['x_acc'], 'X-Axis')
    plot_axis(ax1, data['timestamp'], data['y_acc'], 'Y-Axis')
    plot_axis(ax2, data['timestamp'], data['z_acc'], 'Z-Axis')
    plt.subplots_adjust(hspace=0.2)
    fig.suptitle(activity)
    plt.subplots_adjust(top=0.90)
    plt.show()

def plot_axis(ax, x, y, title):

    ax.plot(x, y, 'r')
    ax.set_title(title)
    ax.xaxis.set_visible(False)
    ax.set_ylim([min(y) - np.std(y), max(y) + np.std(y)])
    ax.set_xlim([min(x), max(x)])
    ax.grid(True)


def plot_confusion_matrix(cm, class_names,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Purples):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    np.set_printoptions(precision=2)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    ax = plt.subplot()
    sns.heatmap(cm, cmap=cmap, annot=True, ax = ax, fmt='.2f'); 
    
    # labels, title and ticks
    ax.set_xlabel('Predicted labels\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass));
    ax.set_ylabel('True labels'); 
    ax.set_title('Confusion Matrix'); 
    ax.xaxis.set_ticklabels(class_names); ax.yaxis.set_ticklabels(class_names, rotation="45");
